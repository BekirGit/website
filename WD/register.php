<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <!--Bootstrap lib-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <!--jQuarry lib-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<style>
body {
  font-family: Arial, Helvetica, sans-serif;
  background-color: black;
}

* {
  box-sizing: border-box;
}

/* Add padding to containers */
.container {
  padding: 16px;
  background-color: white;
}

/* Full-width input fields */
input[type=text], input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  display: inline-block;
  border: none;
  background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}

/* Overwrite default styles of hr */
hr {
  border: 1px solid #f1f1f1;
  margin-bottom: 25px;
}






/* Add a blue text color to links */
a {
  color: dodgerblue;
}

/* Set a grey background color and center the text of the "sign in" section */
.signin {
  background-color: #f1f1f1;
  text-align: center;
}
</style>
</head>
<body>
<a href="../website/homepage.html" class="btn btn-primary btn-lg btn-block" role="button">Zur&uuml;ck zur Homepage</a> 
<form action="insert.php" method="post">
  <div class="container">
    <h1>Register</h1>
    <p>Please fill in this form to create an account.</p>
    <hr>

    <label for="Name"><b>Name</b></label>
    <input type="text" placeholder="Name" name="Name" id="Name" required>

    <label for="Vorname"><b>Vorname</b></label>
    <input type="text" placeholder="Vorname" name="Vorname" id="Vorname" required>

    <label for="Benutzername"><b>Benutzername</b></label>
    <input type="text" placeholder="Benutzername" name="Benutzername" id="Benutzername" required>

    <label for="Adresse"><b>Adresse</b></label>
    <input type="text" placeholder="Adresse" name="Adresse" id="Adresse" required>

    <label for="Geburtstag"><b>Geburtstag</b></label>
    <input type="text" placeholder="jjjj-mm-tt" name="Geburtstag" id="Geburtstag" required>

    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="psw" id="psw" required>
    
    <hr>
    <p>By creating an account you agree to our <a href="#">Terms & Privacy</a>.</p>

    <button type="submit" class="btn btn-primary btn-lg btn-block">Register</button>
  </div>
  
  <div class="container signin">
    <p>Already have an account? <a href="#">Sign in</a>.</p>
  </div>
</form>

</body>
</html>