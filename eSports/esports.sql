-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 08. Jul 2021 um 13:28
-- Server-Version: 10.4.8-MariaDB
-- PHP-Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `esports`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bestellungen`
--

CREATE TABLE `bestellungen` (
  `id` int(15) NOT NULL,
  `Eventdatum` varchar(255) NOT NULL,
  `Eventname` varchar(255) NOT NULL,
  `Eventbeschreibung` varchar(255) NOT NULL,
  `price` int(10) NOT NULL,
  `units` int(5) NOT NULL,
  `total` int(15) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `email` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `bestellungen`
--

INSERT INTO `bestellungen` (`id`, `Eventdatum`, `Eventname`, `Eventbeschreibung`, `price`, `units`, `total`, `date`, `email`) VALUES
(12, '2022-01-01', 'Grand Turismo', 'Erlebe die Spannung von Rennen in den schnellsten Rennwagen.', 20, 1, 20, '2021-07-08 00:17:14', 'test@gmail.com'),
(13, '2022-02-19', 'Dirt Rally', 'Erlebe in den mächtigsten Geländewagen aller Zeiten einen Weg durch Rallye-Standorte.', 10, 1, 10, '2021-07-08 02:23:47', 'test@gmail.com'),
(14, '2022-03-12', 'Super Mario', 'Begib dich mit Mario, Luigi, Peach und Toad auf eine Mission, um das Feenland zu retten!', 10, 2, 20, '2021-07-08 02:23:47', 'test@gmail.com'),
(15, '2022-02-19', 'Dirt Rally', 'Erlebe in den mächtigsten Geländewagen aller Zeiten einen Weg durch Rallye-Standorte.', 10, 4, 40, '2021-07-08 02:32:50', '1@test.com'),
(16, '2022-01-01', 'Grand Turismo', 'Erlebe die Spannung von Rennen in den schnellsten Rennwagen.', 20, 2, 40, '2021-07-08 11:09:07', 'test@gmail.com');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `Eventdatum` date NOT NULL,
  `Eventname` varchar(60) NOT NULL,
  `Eventbeschreibung` tinytext NOT NULL,
  `Eventfoto` varchar(60) NOT NULL,
  `qty` int(5) NOT NULL,
  `price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `events`
--

INSERT INTO `events` (`id`, `Eventdatum`, `Eventname`, `Eventbeschreibung`, `Eventfoto`, `qty`, `price`) VALUES
(1, '2022-01-01', 'Grand Turismo', 'Erlebe die Spannung von Rennen in den schnellsten Rennwagen.', 'Grand_Turismo.jpg', 38, '20.00'),
(2, '2022-02-19', 'Dirt Rally', 'Erlebe in den mächtigsten Geländewagen aller Zeiten einen Weg durch Rallye-Standorte.', 'Dirt_Rally.jpg', 25, '10.00'),
(3, '2022-03-12', 'Super Mario', 'Begib dich mit Mario, Luigi, Peach und Toad auf eine Mission, um das Feenland zu retten!', 'Super_Mario.jpg', 18, '10.00'),
(4, '2022-04-29', 'MotoGP', 'Messen Sie sich bei spannenden Kopf-an-Kopf Rennen mit anderen Fahrern.', 'MotoGP.jpg', 35, '20.00'),
(5, '2022-05-09', 'MXGP', 'Erlebe ein einzigartiges Motocross-Erlebnis der MXGP-Weltmeisterschaft.', 'MXGP.jpg', 25, '15.00'),
(6, '2022-06-20', 'Call of Duty', 'Stürzen Sie sich auf drei neue Mehrspieler-Karten sowie 50 Stufen mit Battle Pass-Belohnungen!', 'Call_of_Duty.jpg', 10, '5.00'),
(7, '2022-07-23', 'FIFA', 'Dich erwartet ein Gameplay-Realismus, der dich für deine Kreativität und deine Kontrolle belohnt.', 'FIFA.jpg', 30, '15.00'),
(8, '2021-08-18', 'NHL', 'Werde zum Superstar und schreibe als einer der besten Spieler der Liga Geschichte.', 'NHL.jpg', 20, '15.00'),
(9, '2021-09-16', 'Tennis World Tour', 'Lege deinen Spielstil und deine Taktik fest und werde Nr. 1 in den Online-Weltranglisten!', 'Tennis.jpg', 25, '10.00');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `bestellungen`
--
ALTER TABLE `bestellungen`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `bestellungen`
--
ALTER TABLE `bestellungen`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT für Tabelle `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
