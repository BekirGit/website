<?php

//if (session_status() !== PHP_SESSION_ACTIVE) {session_start();}
//if(session_id() == '' || !isset($_SESSION)){session_start();}
session_start();
if(!isset($_SESSION['userid'])) {
    die('Bitte zuerst <a href="login.php">einloggen</a>');
}
 
//Abfrage der Nutzer ID vom Login
$userid = $_SESSION['userid'];

include 'config.php';



?>

<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Warenkorb || eSports</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>

    <nav class="top-bar" data-topbar role="navigation">
      <ul class="title-area">
        <li class="name">
          <h1><a href="index.php">eSports Eventplattform</a></h1>
        </li>
        <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
      </ul>

      <section class="top-bar-section">
      <!-- Right Nav Section -->
        <ul class="right">
          <li><a href="index.php">Home</a></li>
          <li><a href="events.php">Events</a></li>
          <li class="active"><a href="warenkorb.php">Warenkorb anzeigen</a></li>
          <li><a href="bestellungen.php">Meine Bestellungen</a></li>
          <li><a href="kontakt.php">Kontakt</a></li>
          <?php

          if(isset($_SESSION['userid'])){
            echo '<li><a href="account.php">Mein Konto</a></li>';
            echo '<li><a href="logout.php">Abmeldung</a></li>';
          }
          else{
            echo '<li><a href="anmeldung.php">Anmeldung</a></li>';
            echo '<li><a href="registrierung.php">Registrierung</a></li>';
          }
          ?>
        </ul>
      </section>
    </nav>




    <div class="row" style="text-align:center; margin-top:10px;">
      <div class="large-12">
        <?php

          echo '<p><h3>Ihr Warenkorb</h3></p>';

          if(isset($_SESSION['cart'])) {

            $total = 0;
            echo '<table>';
            echo '<tr>';
            echo '<th>Eventdatum</th>';
            echo '<th>Eventbezeichnung</th>';
            echo '<th>Anzahl Tickets</th>';
            echo '<th>Ticketpreis in CHF</th>';
            echo '</tr>';
            foreach($_SESSION['cart'] as $product_id => $quantity) {

            $result = $mysqli->query("SELECT Eventdatum, Eventname, Eventbeschreibung, qty, price FROM events WHERE id = ".$product_id);


            if($result){

              while($obj = $result->fetch_object()) {
                $cost = $obj->price * $quantity; //work out the line cost
                $total = $total + $cost; //add to the total cost

                echo '<tr>';
                echo '<td>'.$obj->Eventdatum.'</td>';
                echo '<td>'.$obj->Eventname.'</td>';
                echo '<td>'.$quantity.'&nbsp;<a class="button [secondary success alert]" style="padding:5px;" href="update-cart.php?action=add&id='.$product_id.'">+</a>&nbsp;<a class="button alert" style="padding:5px;" href="update-cart.php?action=remove&id='.$product_id.'">-</a></td>';
                echo '<td>'.$cost.'</td>';
                echo '</tr>';
              }
            }

          }



          echo '<tr>';
          echo '<td colspan="3" align="right">Total</td>';
          echo '<td>'.$total.'</td>';
          echo '</tr>';

          echo '<tr>';
          echo '<td colspan="4" align="right"><a href="update-cart.php?action=empty" class="button alert">Warenkorb leeren</a>&nbsp;<a href="events.php" class="button [secondary success alert]">Einkauf fortsetzen</a>';
          if(isset($_SESSION['userid'])) {
            echo '<a href="bestellungen-update.php"><button style="float:right;">Bestellung</button></a>';
          }

          else {
            echo '<a href="anmeldung.php"><button style="float:right;">Anmeldung</button></a>';
          }

          echo '</td>';

          echo '</tr>';
          echo '</table>';
        }

        else {
          echo "Sie haben keine Artikel in Ihrem Einkaufswagen.";
        }





          echo '</div>';
          echo '</div>';
          ?>



    <div class="row" style="margin-top:10px;">
      <div class="small-12">




        <footer style="margin-top:10px;">
           <p style="text-align:center; font-size:0.8em;clear:both;">&copy; eSports Eventplattform. Alle Rechte vorbehalten.</p>
        </footer>

      </div>
    </div>





    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>
