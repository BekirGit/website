<?php

//if (session_status() !== PHP_SESSION_ACTIVE) {session_start();}
if(session_id() == '' || !isset($_SESSION)){session_start();}

?>

<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Kontakt || eSports</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>

    <nav class="top-bar" data-topbar role="navigation">
      <ul class="title-area">
        <li class="name">
          <h1><a href="index.php">eSports Eventplattform</a></h1>
        </li>
        <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
      </ul>

      <section class="top-bar-section">
      <!-- Right Nav Section -->
        <ul class="right">
          <li><a href="index.php">Home</a></li>
          <li><a href="events.php">Events</a></li>
          <li><a href="warenkorb.php">Warenkorb anzeigen</a></li>
          <li><a href="bestellungen.php">Meine Bestellungen</a></li>
          <li class="active"><a href="kontakt.php">Kontakt</a></li>
          <?php

          if(isset($_SESSION['username'])){
            echo '<li><a href="account.php">Mein Konto</a></li>';
            echo '<li><a href="logout.php">Abmeldung</a></li>';
          }
          else{
            echo '<li><a href="anmeldung.php">Anmeldung</a></li>';
            echo '<li><a href="registrierung.php">Registrierung</a></li>';
          }
          ?>
        </ul>
      </section>
    </nav>




    <div class="row" style="text-align:center; margin-top:60px;">
      <div class="small-12">

        <p>Bei Fragen oder Unklarheiten stehen wir gerne unter folgender E-Mail zur Verfügung: <a href="mailto:support@esports.com">support@esports.com</a></p>
        <img data-interchange="[images/eSports_Frage.jpg, (large)]">

        <footer>
           <p style="text-align:center; font-size:0.8em;">&copy; eSports Eventplattform. Alle Rechte vorbehalten.</p>
        </footer>

      </div>
    </div>







    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>
