<?php

//if (session_status() !== PHP_SESSION_ACTIVE) {session_start();}
//if(session_id() == '' || !isset($_SESSION)){session_start();}
session_start();
if(!isset($_SESSION['userid'])) {
    die('Bitte zuerst <a href="login.php">einloggen</a>');
}
 
//Abfrage der Nutzer ID vom Login
$userid = $_SESSION['userid'];

include 'config.php';



?>

<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Events || eSports</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>

    <nav class="top-bar" data-topbar role="navigation">
      <ul class="title-area">
        <li class="name">
          <h1><a href="index.php">eSports Eventplattform</a></h1>
        </li>
        <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
      </ul>

      <section class="top-bar-section">
      <!-- Right Nav Section -->
        <ul class="right">
          <li><a href="index.php">Home</a></li>
          <li class='active'><a href="events.php">Events</a></li>
          <li><a href="warenkorb.php">Warenkorb anzeigen</a></li>
          <li><a href="bestellungen.php">Meine Bestellungen</a></li>
          <li><a href="kontakt.php">Kontakt</a></li>
          <?php

          if(isset($_SESSION['userid'])){
            echo '<li><a href="account.php">Mein Konto</a></li>';
            echo '<li><a href="logout.php">Abmeldung</a></li>';
          }
          else{
            echo '<li><a href="anmeldung.php">Anmeldung</a></li>';
            echo '<li><a href="registrierung.php">Registrierung</a></li>';
          }
          ?>
        </ul>
      </section>
    </nav>




    <div class="row" style="margin-top:10px;">
      <div class="small-12">
        <?php
          $i=0;
          $product_id = array();
          $product_quantity = array();

          $result = $mysqli->query('SELECT * FROM events');
          if($result === FALSE){
            die(mysql_error());
          }

          if($result){

            while($obj = $result->fetch_object()) {

              echo '<div class="large-4 columns">';
              echo '<p><h3>'.$obj->Eventname.'</h3></p>';
              echo '<img src="images/events/'.$obj->Eventfoto.'"/>';
              echo '<p><strong>Eventdatum</strong>: '.$obj->Eventdatum.'</p>';
              echo '<p><strong>Eventbeschreibung</strong>: '.$obj->Eventbeschreibung.'</p>';
              echo '<p><strong>Anzahl Tickets</strong>: '.$obj->qty.'</p>';
              echo '<p><strong>Ticketpreis</strong>: '.$currency.$obj->price.'</p>';



              if($obj->qty > 0){
                echo '<p><a href="update-cart.php?action=add&id='.$obj->id.'"><input type="submit" value="Zum Warenkorb hinzufügen" style="clear:both; background: #0078A0; border: none; color: #fff; font-size: 1em; padding: 10px;" /></a></p>';
              }
              else {
                echo 'Out Of Stock!';
              }
              echo '</div>';

              $i++;
            }

          }

          $_SESSION['product_id'] = $product_id;


          echo '</div>';
          echo '</div>';
          ?>

        <div class="row" style="margin-top:10px;">
          <div class="small-12">




        <footer style="margin-top:10px;">
           <p style="text-align:center; font-size:0.8em;clear:both;">&copy; eSports Eventplattform. Alle Rechte vorbehalten.</p>
        </footer>

      </div>
    </div>





    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>
