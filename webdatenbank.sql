-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 08. Jul 2021 um 15:59
-- Server-Version: 10.1.28-MariaDB
-- PHP-Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `webdatenbank`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `person`
--

CREATE TABLE `person` (
  `ID_Pers` int(11) NOT NULL,
  `Name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NOT NULL,
  `Vorname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NOT NULL,
  `Benutzername` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NOT NULL,
  `Adresse` varchar(100) CHARACTER SET utf8 COLLATE utf8_german2_ci NOT NULL,
  `Geburtstag` date NOT NULL,
  `psw` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;

--
-- Daten für Tabelle `person`
--

INSERT INTO `person` (`ID_Pers`, `Name`, `Vorname`, `Benutzername`, `Adresse`, `Geburtstag`, `psw`) VALUES
(1, 'Muster', 'Max', 'maxmuster', 'Z?rich', '1980-01-01', '$2y$10$JKHV4pgLC.Okxts2DuZc9O3ZEloVkPTqJ7Y1y/nMOchZt6HqSlDKK'),
(2, 'Hans', 'Muster', 'hansmuster', 'Z?rich', '1980-01-01', '$2y$10$UIbTPzREJAmtSXYuvk98Y.VVeVVmvv6fR256mrXjmrA8BtLP/WwWq'),
(3, 'admin', 'admin', 'admin', 'Datenbank', '1980-01-01', '$2y$10$Y29tNKtcM.xrQzbkBHWB4uxXFmMLF0dZ2Pj8pH6.jSttFugx7seay'),
(4, 'maier', 'samuel', 'masu', 'ZÃ¼rich imiat 1', '1980-01-01', '$2y$10$BUZP8mI8TYsQS1QZT1pu5.XIM31.n5/whjks5pksePXfj6D29sAdi'),
(5, 'Meier', 'Peter', 'mepe', 'ZÃ¼rich imiat 1', '1980-01-01', '$2y$10$hdLdU2LUF4Tc6hKAc8NPHet41ibYwJkfuNlgLowR95gZ8cs8g6fOq');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`ID_Pers`),
  ADD UNIQUE KEY `Benutzername` (`Benutzername`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `person`
--
ALTER TABLE `person`
  MODIFY `ID_Pers` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
